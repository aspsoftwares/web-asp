const express = require('express')
const router = express.Router()
const connection = require('../bd')
// const key = pjson.key


router.get('/get', (req, res, next) => {
    // TODO: validar token
    if(!req.query.key) {res.send({"error":"Chave da API não informada."}).end(); return}
    if(req.query.key !== '5Rn2CzUvWnSSZZTS') {res.send({"error":"Chave da API inválida."}); return}
    if(!req.query.cnpj) {res.send({"error":"CNPJ não informado."}); return}
    if(req.query.cnpj.length < 14) {res.send({"error":"CNPJ incorreto."}); return}
    connection.query('SELECT * from cnpj WHERE cnpj = ?', req.query.cnpj ,function (error, results, fields) {
        if (error) {
            connection.end();
            res.send(error)
        };
        if(results.length == 0){ res.send({"error":"CNPJ não encontrado"}); return }
        connection.query('INSERT INTO cnpj_log SET ?', {"cnpj": req.query.cnpj, "api_key":req.query.key}, (err,response)=>{
            if (err) {
                res.send(err)
            }
            
            res.send(results[0])
        }); 
    }); 
})

module.exports = router