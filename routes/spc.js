const express = require('express')
const router = express.Router()
const connection = require('../bd_mongo')
const spc_log = require('../model/spc')
const axios = require('axios')

let auth = async (req) => {
    return new Promise((resolve,reject)=>{
        if(!req.query.key) {resolve({"error":"Chave da API não informada."})}
        else if(req.query.key !== '5Rn2CzUvWnSSZZTS') {resolve({"error":"Chave da API inválida."})}
        else if(!req.query.cpfcnpj) {resolve({"error":"CNPJ não informado."})}
        else if(!req.query.entity) {resolve({"error":"Entidade não informada."})}
        else if(!req.query.user) {resolve({"error":"Usuario não informada."})}
        else if(!req.query.passwd) {resolve({"error":"Senha não informada."})}
        else{
            resolve(true)
        }
    })
}

let envelope = {
    'data': '',
    'open': async (c)=>{
        return new Promise((resolve,reject)=>{
            c = c + '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:con="http://www.fcdl-sc.org.br/sispc/consulta"><soapenv:Header/><soapenv:Body>'
            resolve(c)
        })
    },
    'close': async (c)=>{
        return new Promise((resolve,reject)=>{
            c = c + '</soapenv:Body></soapenv:Envelope>'
            resolve(c)
        })
    },
    'plusmaster': async (c,cpfcnpj)=>{
        return new Promise((resolve,reject)=>{
            c = c + '<con:SPCPlusMaster-62><filtroConsultaSpcPlusMasterWS><cpfCnpj>'+cpfcnpj+'</cpfCnpj></filtroConsultaSpcPlusMasterWS></con:SPCPlusMaster-62>'
            resolve(c)
        })
    }
}

router.post('/visualizar', async (req,res)=>{
    if(req.body.id){
        spc_log.findOne({_id:req.body.id},(err, data)=>{
            if (err) res.send(err)
            res.send(data.resultado)
        })
    }else{
        res.status(402).send({'Error':'ID não informado!'})
    }
})

router.get('/plusmaster', async (req, res, next) => {
    let authorized = await auth(req)
    if(authorized !== true){
        res.send(authorized)
    }else{
        let Authorization = new Buffer.from(req.query.entity+':'+req.query.user+':'+req.query.passwd).toString('base64')
        let body = ''
        body = await envelope.open(body)
        body = await envelope.plusmaster(body,req.query.cpfcnpj)
        body = await envelope.close(body)
                
        axios.post('https://spc.cdl-sc.org.br/spc-web/ConsultaSpcscService',body,{
            headers: {
                Authorization : 'Basic '+Authorization,
                Accept: 'Text/xml',
                'Content-Type' : 'Text/xml'
            },
        })
        .then((response)=>{
            var parseString = require('xml2js').parseString;
            parseString(response.data, {explicitArray : false, normalizeTags: true}, function (err, result) {
                let data = result['soap:envelope']['soap:body']['ns2:respostaconsultaspcplusmasterresult']['resultado']
                    spc_log.create({apikey:req.query.key, entidade: req.query.entity, usuario:req.query.user, senha: req.query.passwd, resultado:data},(err, success)=>{
                        if (err) console.log(err);
                        data._id = success._id
                        res.send(data)
                    })
            });

        })
        .catch(err => {
            res.send({
                "code": err.message,
                "message": err.response.data
            })
        })
    }
})

module.exports = router