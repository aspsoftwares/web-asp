
# Web ASP

  

## Pagina para a raiz do servidor webasp.com.br


Usuario: webasp

Senha: asp2019@@

## Documentação API's

### CNPJ
#### Buscar por CNPJ

 - GET em 'https://webasp.com.br/api/cnpj/get?cnpj=[CNPJ]&key=[API_Key]'
 - Parametros
	 - cnpj
	 - key (Pedir para @biutas criar uma)

### SPC
#### Consultas disponiveis

* SPC Plus Master (62)

 - GET em 'https://webasp.com.br/api/spc/[TIPO_CONSULTA]?cpfcnpj=[CNPJ]&key=[API_Key]&entity=[ENTIDADE]&user=[USUARIO_SPC]&passwd=[SENHA_SPC]'
 - Parametros
	 - cpfcnpj (CPF/CNPJ para ser buscado)
	 - key (Pedir para @biutas criar uma)
	 - entity (Entidade da CDL, o cliente deve fornecer)
	 - user (Usuario do cliente na CDL)
	 - passwd (Senha do cliente na CDL)


