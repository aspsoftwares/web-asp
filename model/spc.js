var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var spcSchema = new Schema({
    date: {type: Date, default: Date.now},
    apikey: String,
    entidade: String,
    usuario: String,
    senha: String,
    cpfcnpj: String,
    resultado: {},    
}, { collection: 'spc_log'} );

module.exports = mongoose.model("spc_log", spcSchema);
